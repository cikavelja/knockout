function BodyData(){
    let self = this;
    self.rowData = ko.observableArray([]);
    self.unselectedRowData = ko.observableArray([]);
    self.selector = ko.observableArray([]);

    insert(valuesArray, self.rowData); //For adding rows in table
    insert(newValuesArray, self.unselectedRowData);

    function insert(valuesJson, newArray) {
        for( var i = 0; i < valuesJson.length ; i++ ){
            addValue(valuesJson[i], newArray);
        }
    }

    function addValue(value, arr){
        arr.push(new RowData(value.id, value.values));
    }

    self.removeRow = function (row) {
        self.rowData.remove(row);
        self.unselectedRowData.push(row);
    };

    self.addRow = function (){
        let selectedValue = $("#selectCoinsAdd").val();
        let addingRow = ko.utils.arrayFirst(self.unselectedRowData(), function(item) {
            return item.id === selectedValue;
        });
        self.rowData.push(addingRow);
        self.unselectedRowData.remove(addingRow);
    };

    self.refreshRow = function () {
        for(let i = 0; i < refreshValuesArray.length; i++){
            let refreshRows = ko.utils.arrayFirst(self.rowData(), function(item) {
                return item.id === refreshValuesArray[i].id;
            });
            if(refreshRows) {
                self.refreshObject(refreshRows, refreshValuesArray[i].values);
            }
        }
    };

    self.refreshObject = function (obj, newObj) {
        obj.old(newObj[2]);
        obj.new(newObj[3]);
    }
}
ko.applyBindings(new BodyData());
