function RowData(id, values) {
    var self = this;
    self.id = id;
    self.currency = values[0];
    self.symbol = values[1];
    self.old = ko.observable(values[2]);
    self.new = ko.observable(values[3]);
    self.diff = ko.observable();
    self.indexcss = ko.observable();
    self.index = ko.observable();

    self.diff = ko.computed(function() {
        return (self.new() - self.old()).toFixed(2);
    });

    self.indexcss = ko.computed(function() {
        return self.diff() > 0 ? "higher" : "lower";
    });

    self.index = ko.computed(function() {
        return self.diff() > 0 ? "+": "-";
    });
}